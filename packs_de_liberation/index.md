---
vim: et ts=4 sts=4 sw=4
title: les packs de liberation
keywords: [ renater, service public, loi 2016-1321 , république numérique ]
#author: mch
TODO:
    - voir les références qui commencent par todo dans le corps du texte
references:
- id: majouview
  url: https://www.youtube.com/watch?v=TxHqU0ICDr4
---

[repnum]: https://www.legifrance.gouv.fr/dossierlegislatif/JORFDOLE000031589829/

Etant mandaté par RENATER depuis 2017 pour promouvoir et dynamiser la politique de
liberation de la production intellectuelle du GIP, je contaste que malgré une véritable
volonté de changement tant dans les équipes qu'au niveau de nos gouvernances
([loi sur la république numérique][repnum], mission Bothorel, ... ), les
habitudes sont tenaces et nous peinons à intégrer la publication et la
contribution comme une démarche naturelle du processus de développement.
Aussi, il semble que penser la libération comme un bénéfice commun semble
contre-intuitif aux décideurs qui préfèrent les logiques de contractualisations
et les produits sur étagère à la prise de risque que peut constituer le
développement (ou son financement) de l'évolution d'un logiciel libre. Cette
peur constitue à la fois un argument de vente et un modèle commercial.

Je fais pour ma part que la pédagogie complète et l'encouragement à la prise de décision
ne fonctionne qu'auprès de personnes qui ont déjà participé à de telles réflexions et ayant
déjà l'experience des outils méthodologiques.

Cette situation rappelle le long travail pédagogique autour de la qualité des
briques logicielles: le contrôle de versions et la systèmatisation de l'usage
de tests s'est démocratisé uniquement avec l'apparition d'outils qui
(comme gitlab et la CI):

* rendaient les bonnes pratiques plus confortables que les mauvaises
* restructuraient l'organisation des cycles de production autour de ces concepts

Aussi j'aimerais proposer des outils méthodologiques (et techniques dans un second temps)
pour simplifier:

* la prise de décision en amont du développement
* la communication vers les organismes externes pendant le développement

Je propose de définir des "packs" qui constituent une base de décision prête à l'emploi
et qui permette:

* aux équipes techniques de disposer d'une feuille de route claire
* aux "compagnons" de simplifier le discours et les démarches d'accompagnement
* aux décideurs de savoir exactement quels moyens doivent être mis à disposition
  pour quelle attente et quel bénéfice.

Ces services doivent permettre

* simplification du pilotage
    * pour le PO: afficher clairement les ambitions pour son projet
    * pour le décideur: comprendre simplement
      * le niveau d'engagement
      * la duré minimale d'engagement
    * pour le personel technique: disposer d'une checklist à
      valider, commune a toutes les applis et d'une méthode
      possiblement outillée et un budget temps connu.
    * pour l'organisme: disposer possiblement d'un accompagnement
      externe? équipe interu ?
* si accord entre organismes de mutualisation: vocabulaire commun
  comprehensible par le décideur public et le prestataire?
  (ex. pack 2: equipe de packaging debian, alpine, ...)

# packs "auteurs"

## pas libre

rappel est fait à la loi [loi][repnum].

## moindre effort

* la derniere version du code est disponible quelque part
* la DINUM est notifiée (adresse de contact a minima)

## evolution

* le code est versionné et disponible sur une forge accessible
  publiquement (au moins en lecture)
* avoir des depots de metadonnées permettant la découverte collaborative

## colaboration

* le code et l'outils dispose d'une documentation
* si deployable: produire une procédure d'install et/ou de recettes
* si platteforme: produire une notice de soumission

## publicité

engagement de l'organisme sur 5 ans:

site web, news, listes de diff:

l'organisme fournit a son PO:
* possible ouverture de la gouvernance
* un budget minimal pour le sponsoring
* du temps et de la formation a son equipe support
* des moyens pour la com. et le graphisme (traduction, ...)

# communauté

engagement de l'organisme sur 5 ans:

* organisations de formations, hackathons, conférence

# outillage

cet outillage a pour but de pouvoir reprendre les avantages de l'open
innovation:

* iteration rapide (d'autant plus que nous aurons peu ou pas de
  moyens si nous commençons entre nous)
* décentralisation
* évitement de l'autorité technique
    * possibilité aux fédérés de s'outiller différement
    * possibilité aux tiers de mettre à disposition des
      infos sur le même modèle.

les metadonnées relatives aux projets libres sont accessibles sous une
forme textuelle synchronisable (ou sous la forme d'evenements) communicables
a la DINUM (ou l'ABES pour une forme bibliobraphique?).

La DINUM et Renater peuvent fournir des espaces de depot et de
l'accompagnement.

# packs contributeurs

a definir


