sources =\
	index.md\
	packs_de_liberation/index.md
pages = ${sources:%.md=public/%.html}

all: ${pages}
clean:; rm -rf tmp public

tmp:; mkfifo $@
${pages}: tmp
public/%.html: %.md ; install -D tmp $@ & < $< pandoc -s > tmp
