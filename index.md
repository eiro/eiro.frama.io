---
vim: et ts=4 sts=4 sw=4
title: mon site pas perso, pas pro, pas citoyen
keywords: []
#author: mch
---

# pro

# en cours

activités en cours ou très prochaines

* [les packs de liberation](packs_de_liberation/)
* ACV (backbone, messagerie et video) avec liberation du code et des
  données
* la liberation des codes en cours (guichet, ekko, evento, ...)
* les focus comex: libre et convivialité numérique
* geek.tv: la conférence francophone perpetuelle: libristes et ESR
  (et pourquoi pas le reste du secteur public?)
* participation au groupe "outils" du comité de programmes de JRES
* liens avec différentes communautés
* proposition de l'inclusion d'une stratégie pour le libre et la
  décentralisation dans le plan stratégique.
* nouvelle tournée de sensibilisation au libre et a l'écologie en
  interne
* participation aux évenements intra et extra muros ESR
* investissement dans ecoinfo:
  * conférences
  * création de la methode "content first", production du site web
    et du projet "kantwee" sur ce modele.

# asso.

je fais partie d'un collectif qui tente de déployer la convivialité
numérique en alsace. nous sommes en lien avec ARN et desclicks mais
nos projets divergent sur la nécessité de co-construire un réseau
et une informatique neutre: pour nous, il n'y a pas d'utilisateurs:
que des co-gestionnaires.

* liste de fonctionnement: https://strasbourg.linuxfr.org/listes/llug
* annonces de la fédération des assos: https://alsace.netlib.re/

Nous préparons actuellement des activités que nous comptons exporter
dans toute l'alsace

* un world café autour de la projection du film "la bataille du libre"
* install parties
* conférence sur la sobriété et la convivialité numérique

le tout se fait en partenariat avec les jardins des sciences de
l'université de Strasbourg.

